**Sample Kubernets project using Wordpress, Keycloak and Mailhog**

**Dependencies/Requirements**

- Git
- Docker
- Kubernetes (or Kind/Minikube)
- Kubectl
- Ansible
- kubernetes
- Wordpress
- Keycloak
- MySQL
- PostgreSQL




**Clone the project**

`$ git clone https://gitlab.com/himel.aro5/myapp.git`

**Short Description**

Wordpress deployed with MySQL database and Keycloak deployed with PostgreSQL database

For the DB persitent volume configuration storage locations needs to be defined.


**Note**: docker secret file inside the k8sfiles directory for pulling the private docker image.
