<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'mysqlpassword' );

/** Database hostname */
define( 'DB_HOST', 'mysql-service' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Q/q}:UsM[e2(C9ceZeE/TFrXbS|h@xElwttJW?}^(gUr3WxBR_xu9x-+pZ06h(Vy' );
define( 'SECURE_AUTH_KEY',  'DrG)|akVH8x5h`}3YYR?D&E(%MZt0qGyMp@t)#(TJt^FuK:F!Z50E&k>7,z,x3zI' );
define( 'LOGGED_IN_KEY',    'Pe|n^*,}YSW4l(UdM3|,gp;mwf.wV|xw|>m-DB}y-,CT]7GGm(_R03Bx>OeJ7s c' );
define( 'NONCE_KEY',        '#{Dq?/4Rvud/@{qm[@U~Zm$RJGp&o6ncabL-V2Y(%wgW!$xtc@gCUIdMk}}7BHd;' );
define( 'AUTH_SALT',        '*Dp,rP&|39D%0<0z+_+p8 6R7n/Rl 84CR$6,:ZLSeSaA&{r@|lQas#&l8;:O[ft' );
define( 'SECURE_AUTH_SALT', '6.*%JpT5SR3eUs$ucjS/~?Tg_Zc} PDJpb.N_|Ye7A8=B76NMeDR9d[OOSEjmS>L' );
define( 'LOGGED_IN_SALT',   'Y`+b;4H9m_e@~UzStIvYM5K[aEw}MvFppG?_nirF:|me@2z=7Ma![qI@8?oO.)`+' );
define( 'NONCE_SALT',       'Y:6.#`|/S7/%Gk5t@]|<{(!Qo.QDy@;K<_;QI[CGLA@@G^@XlZKN:hT2hp5!r>0&' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
