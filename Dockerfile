#
# Each instruction in this file generates a new layer that gets pushed to your local image cache
#
# The line below states we will base our new image on the Latest Official Ubuntu 
FROM ubuntu:latest

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Dhaka

# Identify the maintainer of an image
LABEL maintainer="himel.aro5@gmail.com"

# Update the image to the latest packages
RUN apt-get update && apt-get upgrade -y

# Install NGINX and php-fpm.
RUN apt-get install nginx -y
RUN apt-get install php-fpm php-mysql php-gd php-xml php-mbstring vim -y

# Edit php.ini file for securing 
RUN sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' /etc/php/8.1/fpm/php.ini

# Copy required files to destinations.
COPY k8sfiles/wordpress-deployment/nginx-tuning-config.conf /etc/nginx/conf.d/
COPY k8sfiles/wordpress-deployment/vhost.conf /etc/nginx/sites-available/default
RUN mkdir -p /etc/nginx/global
COPY k8sfiles/wordpress-deployment/wp-tuning.conf /etc/nginx/global
COPY k8sfiles/wordpress-deployment/php-fpm.conf /etc/nginx/global
COPY k8sfiles/wordpress-deployment/phpinfo.php /var/www/html/
COPY --chown=www-data:www-data k8sfiles/wordpress-deployment/wordpress/ /var/www/html/

#
# Expose port 80
EXPOSE 80


# test
# Last is the actual command to start up NGINX within our Container
#CMD ["nginx", "-g", "daemon off;"]
#On ubuntu/debian images there is also necessary to allow starting recently installed packages by running a Dockerfile RUN command like this:
#RUN echo "exit 0" > /usr/sbin/policy-rc.d
CMD /etc/init.d/php8.1-fpm restart && nginx -g "daemon off;"